# Bactrack

Bateria tracking in fluorescence-only mother machine timeseries. 

# Usage 

Either add the project root folder to your `python path`, or from the 
project root type the following in a terminal: 


```
python -m bactrack
```

This launches the file selection dialog, followed by a frame exclusion 
dialog. 

This lets the user remove problematic frames, such as out of focus frames, that would 
otherwise disrput the tracking. 
