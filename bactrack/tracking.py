"""tracking.py

Tracking

J. Metz <metz.jp@gmail.com>
"""
from collections import defaultdict
import tqdm
import numpy as np
import scipy.spatial as spatial
import skimage.measure as skmeas
import skimage.feature as skfeat
import scipy.ndimage as ndi
from bactrack import (
    # lapjv,
    utility,
)


def track_cells(cells, data, bgs=None, reverse=False):
    if reverse:
        cells = cells[::-1]
        data = data[::-1]
        bgs = bgs[::-1]
    # Initialise frame "a"
    it_cells = iter(cells)
    it_data = iter(data)
    it_bg = iter(bgs)

    cells_a = next(it_cells)
    im_a = next(it_data)
    bg_a = next(it_bg)

    cellsout = [cells_a]
    bgsout = [bg_a]

    tracks = []
    maxid = cells_a.max()
    # Now perform tracking from a->b!
    print("Tracking cells in %d frames..." % len(data), flush=True)
    for t, (cells_b, im_b, bg_b) in tqdm.tqdm(
        enumerate(zip(it_cells, it_data, it_bg)), total=len(data) - 1
    ):

        # mapping = track_cells_between_frames(cells_a, cells_b, im_a, im_b)
        # print(
        #    "Tracking: frame a:", cells_a.max(),
        #    ", frame b:", cells_b.max())
        # print("length of mapping:", len(mapping))
        # print("Max of mapping:", mapping.max())

        # tracks.append(mapping)

        cellsoutnow, bg = track_cells_between_frames(
            cells_a,
            cells_b,
            im_a,
            im_b,
            maxid,
            # debug_time=t,
            bgs=bg_b,
        )
        cellsout.append(cellsoutnow)
        bgsout.append(bg)
        cells_a = cellsoutnow.copy()
        maxid = max(maxid, cellsoutnow.max())
    # return tracks
    if reverse:
        cellsout = cellsout[::-1]
        bgsout = bgsout[::-1]
    return cellsout, bgsout


def track_cells_between_frames(
    cells_a, cells_b, im_a, im_b, maxid, max_dist=10, debug_time=None, bgs=None
):

    if debug_time is not None:
        print("Tracking ", debug_time, "-->", debug_time + 1)

    pos_a = get_state(cells_a, im_a)
    pos_b = get_state(cells_b, im_b)
    ids_a = get_labels(cells_a)

    if debug_time is not None:
        print("ids_a:", ids_a)
        print("labels in:", np.unique(cells_a))

    # Basic tracking
    # Simple min based
    matches12 = skfeat.match_descriptors(
        pos_a, pos_b, metric="euclidean", cross_check=True
    )

    # LapJV Based
    """
    dist = spatial.distance.cdist(pos_a, pos_b)

    ids_b = -1 * np.ones(len(pos_b))
    indsa = np.arange(max(len(pos_a), len(pos_b)))
    ib, cost = lapjv.lapjv(dist, maxcost=max_dist)
    ids_b[ib[cost >= 0]] = ids_a[indsa[cost >= 0]]
    if debug_time is not None:
        print("ids_b before new:", ids_b)
    Nnew = sum(ids_b < 0)
    ids_b[ids_b < 0] = np.arange(Nnew) + maxid + 1
    if debug_time is not None:
        print("ids_b after new:", ids_b)
    """

    # Perform relabelling
    cells_b_out = np.zeros_like(cells_b)
    # for l_old, l_new in enumerate(ids_b, start=1):
    #    cells_b_out[ cells_b == l_old ] = l_new
    for match in matches12:
        cells_b_out[cells_b == (match[1] + 1)] = ids_a[match[0]]
    if bgs is not None:
        bgsout = [np.zeros_like(bg) for bg in bgs]
        for i, bg in enumerate(bgs):
            for match in matches12:
                bgsout[i][bg == (match[1] + 1)] = ids_a[match[0]]
    else:
        bgsout = None

    if debug_time is not None:
        print("labels out:", np.unique(cells_b_out))

    return cells_b_out, bgsout


def track_channels(channels, maximum_fails_in_a_row=4):
    """
    Tracks channel regions between all frames.

    Uses basic (greedy) cross-checked closest neighbour assignment
    """
    fails_in_a_row = 0
    it_channels = iter(channels)
    Nframes = len(channels)

    channel = next(it_channels)
    channel, _ = ndi.label(channel)
    channels_out = [channel]
    pos_a = np.array([prop.centroid for prop in skmeas.regionprops(channel)])
    ids_a = get_labels(channel)

    for channel in tqdm.tqdm(it_channels, total=Nframes - 1):

        if len(ids_a) < 2:
            print("IDS A is length ", len(ids_a))
            print(ids_a)
            import matplotlib.pyplot as plt

            plt.switch_backend("qt5agg")
            plt.figure("Channel:")
            plt.imshow(channel)
            plt.figure("Previous channel (end of channels_out)")
            plt.imshow(channels_out[-1])
            plt.figure("Previous previous channel (end of channels_out)")
            plt.imshow(channels_out[-2])
            plt.show()

        channel, _ = ndi.label(channel)
        pos_b = np.array(
            [prop.centroid for prop in skmeas.regionprops(channel)]
        )
        matches12 = skfeat.match_descriptors(
            pos_a, pos_b, metric="euclidean", cross_check=True
        )
        try:
            if matches12.shape[0] < 2:
                raise Exception(
                    "Tracking didn't track both channels,"
                    " signals poor tracking"
                )
            channel_out = np.zeros_like(channel)
            for match in matches12:
                channel_out[channel == (match[1] + 1)] = ids_a[match[0]]
            fails_in_a_row = 0
        except Exception:
            fails_in_a_row += 1
            if fails_in_a_row > maximum_fails_in_a_row:
                raise Exception(
                    "Too many channel tracking failures in a row [{}]".format(
                        fails_in_a_row
                    )
                )
            # Very basic strategy, simply append last tracking
            # result that succeeded
            channel_out = channels_out[-1].copy()
        ids_a = get_labels(channel_out)
        if len(ids_a) < 2:
            print("IDS A is length ", len(ids_a))
            import matplotlib.pyplot as plt

            plt.figure("Channel_out:")
            plt.imshow(channel_out)
            plt.show()
        pos_a = pos_b.copy()
        channels_out.append(channel_out)
    return channels_out


def get_state(cell_image, intensity_image):
    # Via skimage
    state = [prop.centroid for prop in skmeas.regionprops(cell_image)]
    return np.array(state)


def get_labels(label_image):
    return np.array(
        [prop.label for prop in skmeas.regionprops(label_image)], dtype="int16"
    )


def filter_tracks_by_length(cells, bgs=[], minlength=1):
    """
    Filter trajectories by length

    As new trajectories are not creates, we can simply inspect which
    trajectories remain in frame `minlength` for minimum length filtering!
    """
    # Minlength filtering
    if minlength == -1:
        # Full length
        minlength = len(cells)
    elif (minlength > 0) and (minlength < 1):
        # Percentage
        minlength = int(minlength * len(cells))
    elif (minlength == 0) or (minlength == 1):
        # All
        return cells
    utility.logger.info(
        "Removing trajectories shorter than {} frames".format(minlength)
    )

    lengths = defaultdict(lambda: 0)
    for frame in cells:
        ids = np.unique(frame[frame > 0])
        for idnow in ids:
            lengths[idnow] += 1

    num_initial = len(lengths)

    idsgood = [idnow for idnow in lengths if lengths[idnow] >= minlength]

    num_final = len(idsgood)
    utility.logger.info(
        "Removed {} trajectories, {} remaining".format(
            num_initial - num_final, num_final
        )
    )

    newlabels = np.arange(1, len(idsgood) + 1, dtype=frame.dtype)
    relabeller = np.zeros(np.max(cells) + 1, dtype=frame.dtype)
    relabeller[idsgood] = newlabels

    for frame_index, cellsnow in enumerate(cells):
        cells[frame_index] = relabeller[cellsnow]
    for frame_index, bgpair in enumerate(bgs):
        for ind, bgnow in enumerate(bgpair):
            bgs[frame_index][ind] = relabeller[bgnow]
    return cells, bgs


def filter_tracks_by_length_basic(cells, bgs=[], minlength=1):
    """
    Filter trajectories by length

    As new trajectories are not creates, we can simply inspect which
    trajectories remain in frame `minlength` for minimum length filtering!
    """
    # Minlength filtering
    frame = cells[minlength]
    idsgood = np.unique(frame[frame > 0])
    newlabels = np.arange(1, len(idsgood) + 1, dtype=frame.dtype)
    relabeller = np.zeros(np.max(cells[0]) + 1, dtype=frame.dtype)
    relabeller[idsgood] = newlabels

    for frame_index, cellsnow in enumerate(cells):
        cells[frame_index] = relabeller[cellsnow]
    for frame_index, bgpair in enumerate(bgs):
        for ind, bgnow in enumerate(bgpair):
            bgs[frame_index][ind] = relabeller[bgnow]
    return cells, bgs


def analyse_well_region_motion(wells, data):
    """
    Basic well region motion analysis
    """
    vels = np.zeros((len(wells) - 1, 2))
    it_wells = iter(wells)
    pos0 = get_single_region_centroid(next(it_wells))
    for tindex, well in enumerate(it_wells):
        pos = get_single_region_centroid(well)
        vels[tindex] = pos - pos0
        pos = pos0.copy()

    import matplotlib.pyplot as plt

    plt.switch_backend("qt5agg")
    plt.figure("Well region speed")
    plt.plot(np.sqrt(np.sum(vels ** 2, axis=1)))
    import matplotlib.ticker as pltticker

    loc = pltticker.MultipleLocator(base=5)
    plt.gca().xaxis.set_major_locator(loc)
    loc = pltticker.MultipleLocator(base=1)
    plt.gca().xaxis.set_minor_locator(loc)
    plt.grid(which="major", linestyle="--", lw=2)
    plt.grid(which="minor", linestyle=":", lw=1)
    plt.show(block=False)
    from skimage.viewer import CollectionViewer

    datavis = data.copy() / data.max()
    for tindex, mask in enumerate(wells):
        perim = (~ndi.binary_erosion(mask)) & mask
        datavis[tindex][perim] = np.min(
            [
                datavis[tindex][perim] + 0.2,
                np.ones(len(datavis[tindex][perim])),
            ],
            axis=0,
        )
    view = CollectionViewer(datavis)
    view.show()


def get_single_region_centroid(mask):
    return np.array(skmeas.regionprops(mask.astype("uint8"))[0].centroid)
