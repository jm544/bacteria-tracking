"""main.py

Main Calling functions for bactrack

J. Metz <metz.jp@gmail.com>
"""

import os
import numpy as np
from bactrack import (
    dataio as btio,
    detection as btdetect,
    tracking as bttrack,
    analysis as btanalysis,
    plotting as btplot,
    ui as btui,
    utility,
)


def process_dataset(
        filenames,
        tmax=None,
        debug=False,
        skip_timepoints=(),
        reverse=False,
        mask=None,
        ):
    """
    Main processing function
    """
    if (
        isinstance(filenames, (list, tuple))
        and (len(filenames) == 1)
        and not filenames[0]
    ) or (not filenames):
        raise ValueError("Filename(s) must be provided")

    data = btio.load_data(filenames, tmax=tmax)

    utility.logger.info("Loaded dataset")
    utility.logger.info(str(filenames))
    utility.logger.info("Of initial shape", data.shape)

    if isinstance(filenames, str):
        output_base = "%s_" % filenames
    else:
        output_base = "%s_" % filenames[0]

    output_root = output_base + "results"

    if not os.path.isdir(output_root):
        os.makedirs(output_root)

    if debug:
        debugfolder = output_base + "debug"
        if not os.path.isdir(debugfolder):
            os.makedirs(debugfolder)
    else:
        debugfolder = None

    time_axis = np.arange(len(data))

    if skip_timepoints:
        utility.logger.info("Skipping timepoints: %s" % str(skip_timepoints))
        time_axis = [
            tpoint for tpoint in time_axis if tpoint not in skip_timepoints
        ]
        data = data[time_axis]

    cells0, wells, channels, bgs, debug_output = \
        btdetect.detect_channel_and_cells(
            data, mask=mask, debugfolder=debugfolder
        )
    # bttrack.analyse_well_region_motion(wells, data)

    wells0 = debug_output.get("wells0", None)

    stats_channel = btanalysis.analyse_channel_fluorescence(
        data, channels, taxis=time_axis
    )

    btplot.plot_channel_stats(stats_channel, output_root)

    cells, bgs = bttrack.track_cells(cells0, data, bgs=bgs, reverse=reverse)

    if debug:
        btplot.output_cell_images_pil(
            data,
            cells,
            os.path.join(debugfolder, "tracking_pre_filter"),
            vmin=1,
            vmax=max(c.max() for c in cells),
            text=True,
            wells=wells,
            wells0=wells0,
            taxis=time_axis,
        )

    cells, bgs = bttrack.filter_tracks_by_length(cells, bgs=bgs, minlength=0.1)

    btio.save_stack(
        cells, os.path.join(output_root, "tracked_cells_stack.tif")
    )

    stats = btanalysis.analyse_cells(
        cells, bgs, data, dark_count_correction=500, taxis=time_axis
    )

    btplot.plot_stats(stats, output_root)

    btio.save_stats(
        stats,
        os.path.join(output_root, "results_table.csv"),
        channel_stats=stats_channel,
    )

    if debug:
        print_stat_summary(stats, total_length=len(data))

    btplot.output_cell_images_pil(
        data,
        cells,
        os.path.join(output_root, "results_tracking"),
        vmin=1,
        vmax=max(c.max() for c in cells),
        text=True,
        wells=wells,
        wells0=wells0,
        taxis=time_axis,
    )


def run_gui(
        filenames=[],
        dev=False,
        debug=False,
        skip_timepoints=None,
        reverse=False,
        mask=None,
        ):
    btui.run_ui(
        filenames,
        dev=dev,
        debug=debug,
        skip_timepoints=skip_timepoints,
        reverse=reverse,
    )


def print_stat_summary(stats, total_length):
    for frac in (0.6, 0.7, 0.8, 0.9, 1.0):
        n = sum(
            1
            for v in stats.values()
            if len(next(iter(v.values()))) >= (frac * total_length)
        )
        print("There are %d stats >= %d" % (n, int(frac * 100)))
