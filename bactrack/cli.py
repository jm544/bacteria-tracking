"""cli.py

Command line interface to bactrack

J. Metz <metz.jp@gmail.com>
"""
import argparse
import re
from bactrack import main as btmain, dev as btdev, utility, dataio as btio
import bactrack.ui.qt_widgets as gui


def create_parser():
    """
    Creates an ArgumentParser instance with
        filenames
        --dev
        --gui
        --tmax
    """
    parser = argparse.ArgumentParser()
    parser.add_argument("filenames", nargs="*", help="Dataset to run")
    parser.add_argument(
        "--dev", help="Run in development mode", action="store_true"
    )
    parser.add_argument(
        "--skip",
        help='List of time points to skip, e.g.  "3,6,10"',
        default="",
        type=str,
    )
    parser.add_argument("--debug", help="Debugging mode", action="store_true")
    parser.add_argument(
        "-r",
        "--reverse",
        help="Reverse mode; track from front to back",
        action="store_true",
    )
    parser.add_argument(
        "-g", "--gui", help="[UNSTABLE] Run in gui mode", action="store_true"
    )
    parser.add_argument(
        "--manual-mask",
        help="Manually supply or create the well region mask",
        action="store_true",
    )
    parser.add_argument("--tmax", help="Max number of frames to run", type=int)
    return parser


# =========================================================
# Main CLI entry point
# =========================================================


def run_cli():
    """
    Runs the command line interface
    """
    parser = create_parser()
    args = parser.parse_args()

    if args.debug or args.dev:
        utility.logger.setLevel(utility.logging.DEBUG)

    # Split on comma OR (multiple) space; also allow spaces around comma
    try:
        timepoints = [
            int(num) for num in re.split("[ ]+|[ ]*,[ ]*", args.skip) if num
        ]
    except Exception:
        raise Exception(
            "Failed to parse time-skip string : %s" % (args.skip)
            + "\nFormat should be : 1,3,5"
        )

    if args.dev:
        # Test 1 dataset
        # filenames = btdev.get_test_data_filenames()
        # filenames = btdev.get_test_data_region0_filenames()
        filenames = btdev.get_test_data_region2()
        # development - need quick turnaround
        # filenames = filenames[:30]
    else:
        filenames = args.filenames

    if not filenames:
        # filenames = get_filenames_gui()
        filenames, timepoints = (
            gui.select_filenames_and_optionally_select_frames_gui()
        )

    if args.manual_mask:
        data = btio.load_data(filenames, tmax=2)
        mask = btio.get_mask(data, filenames[0], save=True, load=True)
    else:
        mask = None

    if args.gui:
        btmain.run_gui(
            filenames,
            dev=args.dev,
            debug=args.debug,
            skip_timepoints=timepoints,
            reverse=args.reverse,
            mask=mask,
        )
    else:
        btmain.process_dataset(
            filenames,
            tmax=args.tmax,
            debug=args.debug,
            skip_timepoints=timepoints,
            reverse=args.reverse,
            mask=mask,
        )
