"""detection.py

Detection

J. Metz <metz.jp@gmail.com>
"""

import os
import tqdm
import math
import numpy as np
from skimage import (
    feature as skfeat,
    filters as skfilt,
    morphology as skmo,
    measure as skmeas,
)
import scipy.ndimage as ndi
import scipy.interpolate
from sklearn.decomposition import PCA
from sklearn.ensemble import IsolationForest
from PIL import Image, ImageDraw
from bactrack import dataio, plotting, utility


def detect_channel_and_cells(data, mask=None, debugfolder=None):
    if mask is None:
        return detect_channel_and_cells_fully_automatic(
            data, debugfolder=debugfolder
        )
    else:
        return detect_channel_and_cells_manual_wells(
            data, mask, debugfolder=debugfolder
        )


def detect_channel_and_cells_manual_wells(data, well, debugfolder=None):
    utility.logger.info(
        "Detecting cells in %d frames..." % len(data))
    cells = []
    bgs = []
    channels = []
    wells = []
    wells0 = []

    axis = determine_well_axis_from_manual_mask(well)

    channel = get_channel_from_well_mask_and_axis(axis, well, data[0].shape)

    for tind, img in enumerate(tqdm.tqdm(data)):
        cell, filt = detect_cells(img, mask=well)
        # Basic marker based watershed
        footprint = np.ones((3, 3))
        local_maxi = skfeat.peak_local_max(
            filt, indices=False, footprint=footprint, labels=cell
        )
        markers, Nmarkers = ndi.label(local_maxi)
        cell = skmo.watershed(-filt, markers, mask=cell).astype("int16")
        bg = get_cell_background_masks(cell, axis)
        cells.append(cell)
        wells.append(well)
        wells0.append(well)
        bgs.append(bg)
        channels.append(channel)
        if debugfolder is not None:
            filename = os.path.join(
                debugfolder,
                "detect_channel_and_cells_output_{:06}.jpg".format(tind),
            )
            plotting.save_detection_overlay(
                filename, img, cell, channel, well=well, well0=well
            )

    if debugfolder is not None:
        dataio.save_stack(
            255 * np.array(wells, dtype="uint8"),
            os.path.join(debugfolder, "wells_stack.tif"),
        )

    return cells, wells, channels, bgs, {"wells0": wells0}


def detect_channel_and_cells_fully_automatic(data, debugfolder=None):
    utility.logger.info(
        "Detecting well region, channel, and cells in %d frames..."
        % len(data)
    )
    cells = []
    wells = []
    wells0 = []
    bgs = []
    channels = []

    well_vectors = determine_well_axes_overtime(data)

    for tind, (img, vec) in enumerate(tqdm.tqdm(zip(data, well_vectors))):
        cell, channel, well, well0, bg = detect_channel_and_cells_in_im(
            img, axis=vec
        )
        cells.append(cell)
        wells.append(well)
        wells0.append(well0)
        bgs.append(bg)
        channels.append(channel)
        if debugfolder is not None:
            filename = os.path.join(
                debugfolder,
                "detect_channel_and_cells_output_{:06}.jpg".format(tind),
            )
            plotting.save_detection_overlay(
                filename, img, cell, channel, well=well, well0=well0
            )

    if debugfolder is not None:
        dataio.save_stack(
            255 * np.array(wells, dtype="uint8"),
            os.path.join(debugfolder, "wells_stack.tif"),
        )

    return cells, wells, channels, bgs, {"wells0": wells0}


def detect_channel_and_cells_in_im(image, axis=None):
    """
    Detect cells in the current frame, optionally using axis provided
    """

    mask_init, filt = detect_cells(image)

    wells, wells0, axis, channel = filter_out_non_periodic_regions(
        mask_init, axis=axis
    )

    mask = mask_init & wells

    # Basic marker based watershed
    footprint = np.ones((3, 3))
    local_maxi = skfeat.peak_local_max(
        filt, indices=False, footprint=footprint, labels=mask
    )
    markers, Nmarkers = ndi.label(local_maxi)
    cells = skmo.watershed(-filt, markers, mask=mask).astype("int16")

    bg_markers = get_cell_background_masks(cells, axis)

    wells0 = mask_init + 2 * wells0

    return cells, channel, wells, wells0, bg_markers


def detect_cells(image, mask=None):
    """
    Basic cell (blob) detection in a specific scale range
    """
    # filt = filt3d.max(axis=-1)
    filt = scale_space_dog(image, min_sigma=2, max_sigma=4).max(axis=-1)

    # thresh = skfilt.threshold_li(filt)
    # thresh = skfilt.threshold_otsu(filt)
    # thresh = threshold_mad(filt)
    thresh = skfilt.threshold_triangle(filt)

    if mask is not None:
        mask_init = (filt > thresh) & mask
    else:
        mask_init = filt > thresh
    return mask_init, filt


def determine_well_axes_overtime(data):
    utility.logger.info("Determining well axis over time...")
    vecs = []
    for image in tqdm.tqdm(data, total=len(data)):
        mask, filt = detect_cells(image)
        labels, Nobjects = ndi.label(mask)
        props = skmeas.regionprops(labels, coordinates="xy")
        vec = determine_wells_axis(props)[0]
        vecs.append(vec)
    vecs = np.array(vecs)
    # Now filter outliers
    outlier_detector = IsolationForest(behaviour="new", contamination="auto")
    outlier_detector.fit(vecs)
    y_pred = outlier_detector.fit(vecs).predict(vecs)
    # Assume smaller group is the outliers
    groups = (y_pred + 1) // 2

    # Interpolate group 0 - the outliers
    taxis_full = np.arange(len(groups))
    taxis = taxis_full[groups == 1]
    vecs_good = vecs[groups == 1]
    interpolator = scipy.interpolate.interp1d(
        taxis, vecs_good, kind="quadratic", fill_value="extrapolate", axis=0
    )
    vecs = interpolator(taxis_full)

    return vecs


def determine_well_axis_from_manual_mask(mask):
    """
    Very basic, uses ellipse fitting
    """
    prop = skmeas.regionprops(mask.astype(int))[0]
    return np.array((np.cos(prop.orientation), np.sin(prop.orientation)))


def scale_space_log(
    image, min_sigma=1, max_sigma=50, num_sigma=10, log_scale=False
):
    """
    Uses the guts of the skimage.blobs_log function to return the scale space
    representation of the image (LoG)
    """

    if log_scale:
        start, stop = np.log10(min_sigma), np.log10(max_sigma)
        sigma_list = np.logspace(start, stop, num_sigma)
    else:
        sigma_list = np.linspace(min_sigma, max_sigma, num_sigma)

    # computing gaussian laplace
    # s**2 provides scale invariance
    gl_images = [
        -1 * ndi.gaussian_laplace(image, s) * s ** 2 for s in sigma_list
    ]
    image_cube = np.dstack(gl_images)
    return image_cube


def scale_space_dog(image, min_sigma=1, max_sigma=50, sigma_ratio=1.6):
    """
    Uses the guts of the skimage.blobs_dog function to return the scale space
    representation of the image (LoG)
    """
    # k such that min_sigma*(sigma_ratio**k) > max_sigma
    k = int(math.log(float(max_sigma) / min_sigma, sigma_ratio)) + 1

    # a geometric progression of standard deviations for gaussian kernels
    sigma_list = np.array(
        [min_sigma * (sigma_ratio ** i) for i in range(k + 1)]
    )

    gaussian_images = [ndi.gaussian_filter(image, s) for s in sigma_list]

    # computing difference between two successive Gaussian blurred images
    # multiplying with standard deviation provides scale invariance
    dog_images = [
        (gaussian_images[i] - gaussian_images[i + 1]) * sigma_list[i]
        for i in range(k)
    ]
    image_cube = np.dstack(dog_images)
    return image_cube


def detect_wells(data):
    """
    Use basic filtering to perform well detection
    """
    allpoints = []

    for i in range(3):
        t = i * (len(data) - 1) // 2
        # ind = i*3
        im = data[t]
        filt = scale_space_dog(im, min_sigma=1, max_sigma=4).max(axis=-1)
        thresh = skfilt.threshold_li(filt)
        mask = filt >= thresh
        labels, Nlabels = ndi.label(mask)
        props = skmeas.regionprops(labels, coordinates="xy")
        # Filter props; min major_axis_length = 50
        # props = [p for p in props if p.major_axis_length >= 50]
        # light filtering, 20...
        props = [p for p in props if p.major_axis_length >= 20]

        # angles = np.array([prop.orientation for prop in props])
        coms = np.array([prop.centroid for prop in props])
        lengths = np.array([prop.major_axis_length for prop in props])
        vecs = np.array(
            [[-np.sin(p.orientation), np.cos(p.orientation)] for p in props]
        )

        # Generate point cloud
        points = np.concatenate(
            [lengths[..., None] * vecs, -lengths[..., None] * vecs]
        )
        allpoints.append(points)
        pca = PCA(n_components=2)
        pca.fit(points)

        # p0 = np.array([size//2 for size in data[t].shape])
        # Show points projected along principal axis and orthogonal axis
        vec = pca.components_[0]

        # Filter out anything with |cosine similarity| < 0.9
        norm = np.sqrt((points * points).sum(axis=1))
        mask = norm > 0
        similarities = np.zeros(norm.shape)
        similarities[mask] = (np.multiply(vec, points).sum(1))[mask] / norm[
            mask
        ]
        keep_mask = similarities >= 0.9
        # Keep mask is double the length of original points as it
        # is formed from both direction of vectors
        keep_coms_mask = (
            keep_mask[:len(keep_mask) // 2] | keep_mask[len(keep_mask) // 2:]
        )

        keep_mask = np.repeat(keep_coms_mask, 2, axis=0)

        points = points[keep_mask]
        coms = coms[keep_coms_mask]

        pos_perp = np.multiply(
            [vec[1], -vec[0]], points + np.repeat(coms, 2, axis=0)
        ).sum(axis=1)
        minsep = 10
        seps = np.diff(np.sort(pos_perp))
        seps = seps[seps >= minsep]
        # seps_med = np.median(seps)
        # seps_norm = seps / seps_med

    pca = PCA(n_components=2)
    allpoints = np.concatenate(allpoints)
    pca.fit(allpoints)


def threshold_mad(data, kvalue=3):
    med = np.median(data)
    mad = np.median(np.abs(data - med))
    return med + kvalue * mad


def filter_out_non_periodic_regions(mask, axis=None):
    """
    Uses fact that cells should appear in regularly spaced wells, and that
    cells in same well should have same position perpendicular to
    wells axis
    """
    labels, Nobjects = ndi.label(mask)
    props = skmeas.regionprops(labels, coordinates="xy")
    if axis is None:
        axis, mask_good = determine_wells_axis_and_good_mask(
            mask.copy(), labels, props
        )
    else:
        mask_good = determine_good_mask(axis, labels)

    lower_pos_med, upper_pos_med = get_lower_and_upper_extents_from_cells(
        axis, mask_good
    )

    wells_mask, channel = generate_wells_and_channel_masks_from_extents(
        axis, (lower_pos_med, upper_pos_med), mask.shape
    )

    # return wells_mask, mask_good, axis, channel, axis
    return wells_mask, mask_good, axis, channel


def determine_wells_axis(props):
    """
    Use orientations of bacteria to determine well axis
    """
    # First filter out any regions that are too low
    # eccentricity to be useful
    MIN_ECCENTRICITY = 0.8
    props = [p for p in props if p.eccentricity >= MIN_ECCENTRICITY]

    # coms = np.array([prop.centroid for prop in props])
    lengths = np.array([prop.major_axis_length for prop in props])
    vecs = np.array(
        [[-np.sin(p.orientation), np.cos(p.orientation)] for p in props]
    )
    points = np.concatenate(
        [lengths[..., None] * vecs, -lengths[..., None] * vecs]
    )
    pca = PCA(n_components=2)
    pca.fit(points)

    # Show points projected along principal axis and orthogonal axis
    vec = pca.components_[0]

    # Normalize
    vec = vec / np.sqrt(np.sum(vec ** 2))
    return vec, props, points


def determine_wells_axis_and_good_mask(mask, labels, props):
    """
    Use orientations of bacteria to determine well axis
    """
    vec = determine_wells_axis(props)
    mask = determine_good_mask(vec, labels)
    return mask, vec


def determine_good_mask(vec, labels):
    MIN_ECCENTRICITY = 0.8
    props = skmeas.regionprops(labels)
    props = [p for p in props if p.eccentricity >= MIN_ECCENTRICITY]

    # coms = np.array([prop.centroid for prop in props])
    lengths = np.array([prop.major_axis_length for prop in props])
    vecs = np.array(
        [[-np.sin(p.orientation), np.cos(p.orientation)] for p in props]
    )
    points = np.concatenate(
        [lengths[..., None] * vecs, -lengths[..., None] * vecs]
    )

    # Filter out anything with |cosine similarity| < 0.9
    norm = np.sqrt((points * points).sum(axis=1))
    mask = norm > 0
    similarities = np.zeros(norm.shape)
    similarities[mask] = (np.multiply(vec, points).sum(1))[mask] / norm[mask]
    keep_mask = similarities >= 0.9
    # Keep mask is double the length of original points as it is formed from
    # both direction of vectors
    keep_coms_mask = (
        keep_mask[:len(keep_mask) // 2] | keep_mask[len(keep_mask) // 2:]
    )

    # Remove labels
    mask = np.zeros(labels.shape, bool)
    for prop, keep in zip(props, keep_coms_mask):
        if keep:
            mask[labels == prop.label] = 1

    return mask


def get_lower_and_upper_extents_from_cells(axis, mask):
    """
    Return an array of upper and lower extents of each
    region's points projected onto axis
    """
    all_points = [
        prop.coords
        for prop in skmeas.regionprops(ndi.label(mask)[0], coordinates="xy")
    ]
    all_pos = [np.multiply(points, axis).sum(axis=1) for points in all_points]
    max_extents = np.array([np.max(pos) for pos in all_pos])
    min_extents = np.array([np.min(pos) for pos in all_pos])

    lower_pos_med = np.median(min_extents)
    upper_pos_med = np.median(max_extents)

    return lower_pos_med, upper_pos_med


def get_lower_and_upper_extents_from_well_mask(axis, mask):
    """
    Return the extents
    """
    points = skmeas.regionprops(ndi.label(mask)[0], coordinates="xy")[0].coords
    pos = np.multiply(points, axis).sum(axis=1)
    max_extent = np.max(pos)
    min_extent = np.min(pos)

    return min_extent, max_extent


def generate_wells_and_channel_masks_from_extents(
    axis,
    extents,
    shape,
    wells_border=40,
    channels_border=80,
    channels_width=40,
):
    """
    Use estimated wells-region axis and extents to generate a
    rectangle bounding the region (with a border).

    Use the rectangle to 'draw' a mask.
    """
    maxshape = np.max(shape)
    # Generate lines using axis and extents
    axis_perp = np.array([-1 * axis[1], axis[0]])
    plower_extent = (extents[0] - wells_border) * axis
    pupper_extent = (extents[1] + wells_border) * axis
    # Generate corners
    pt_ll = plower_extent - 2 * maxshape * axis_perp
    pt_lr = plower_extent + 2 * maxshape * axis_perp
    pt_ur = pupper_extent + 2 * maxshape * axis_perp
    pt_ul = pupper_extent - 2 * maxshape * axis_perp

    coords = np.array((pt_ll[::-1], pt_lr[::-1], pt_ur[::-1], pt_ul[::-1]))
    coords = coords.round().astype("int").flatten().tolist()

    # Draw the polygon
    well_mask = create_polygons_mask(shape, [coords])

    # Add the channel lines
    # Generate lines using axis and extents
    plower_0_extent = (extents[0] - channels_border) * axis
    plower_1_extent = (extents[0] - channels_border - channels_width) * axis
    pupper_0_extent = (extents[1] + channels_border) * axis
    pupper_1_extent = (extents[1] + channels_border + channels_width) * axis
    # Generate corners lower
    pt_lower_ll = plower_0_extent - 2 * maxshape * axis_perp
    pt_lower_lr = plower_0_extent + 2 * maxshape * axis_perp
    pt_lower_ur = plower_1_extent + 2 * maxshape * axis_perp
    pt_lower_ul = plower_1_extent - 2 * maxshape * axis_perp

    coords_lower = np.array(
        (
            pt_lower_ll[::-1],
            pt_lower_lr[::-1],
            pt_lower_ur[::-1],
            pt_lower_ul[::-1],
        )
    )
    coords_lower = coords_lower.round().astype("int").flatten().tolist()
    # Generate corners upper
    pt_upper_ll = pupper_0_extent - 2 * maxshape * axis_perp
    pt_upper_lr = pupper_0_extent + 2 * maxshape * axis_perp
    pt_upper_ur = pupper_1_extent + 2 * maxshape * axis_perp
    pt_upper_ul = pupper_1_extent - 2 * maxshape * axis_perp

    coords_upper = np.array(
        (
            pt_upper_ll[::-1],
            pt_upper_lr[::-1],
            pt_upper_ur[::-1],
            pt_upper_ul[::-1],
        )
    )

    coords_upper = coords_upper.round().astype("int").flatten().tolist()

    channel_mask = create_polygons_mask(shape, [coords_lower, coords_upper])

    return well_mask, channel_mask


def get_channel_from_well_mask_and_axis(
    axis, well, shape, wells_border=40, channels_border=80, channels_width=40
):
    """
    Given a well mask, and an axis, create channel mask
    """
    # For consitency with other approach
    channels_border -= wells_border
    maxshape = np.max(shape)
    axis_perp = np.array([-1 * axis[1], axis[0]])
    extents = get_lower_and_upper_extents_from_well_mask(axis, well)
    # Add the channel lines
    # Generate lines using axis and extents
    plower_0_extent = (extents[0] - channels_border) * axis
    plower_1_extent = (extents[0] - channels_border - channels_width) * axis
    pupper_0_extent = (extents[1] + channels_border) * axis
    pupper_1_extent = (extents[1] + channels_border + channels_width) * axis

    # Generate corners lower
    pt_lower_ll = plower_0_extent - 2 * maxshape * axis_perp
    pt_lower_lr = plower_0_extent + 2 * maxshape * axis_perp
    pt_lower_ur = plower_1_extent + 2 * maxshape * axis_perp
    pt_lower_ul = plower_1_extent - 2 * maxshape * axis_perp

    coords_lower = np.array(
        (
            pt_lower_ll[::-1],
            pt_lower_lr[::-1],
            pt_lower_ur[::-1],
            pt_lower_ul[::-1],
        )
    )
    coords_lower = coords_lower.round().astype("int").flatten().tolist()
    # Generate corners upper
    pt_upper_ll = pupper_0_extent - 2 * maxshape * axis_perp
    pt_upper_lr = pupper_0_extent + 2 * maxshape * axis_perp
    pt_upper_ur = pupper_1_extent + 2 * maxshape * axis_perp
    pt_upper_ul = pupper_1_extent - 2 * maxshape * axis_perp

    coords_upper = np.array(
        (
            pt_upper_ll[::-1],
            pt_upper_lr[::-1],
            pt_upper_ur[::-1],
            pt_upper_ul[::-1],
        )
    )

    coords_upper = coords_upper.round().astype("int").flatten().tolist()

    channel_mask = create_polygons_mask(shape, [coords_lower, coords_upper])

    return channel_mask


def create_polygons_mask(shape, polygons):
    # Draw the polygon
    img = Image.new("1", shape[::-1])
    imgdraw = ImageDraw.Draw(img)
    for coords in polygons:
        imgdraw.polygon(coords, fill=1)
    return np.array(img)


def get_cell_background_masks(
    cells, axis, min_sep=10, dist_width=1, bgwidth=2
):
    """
    Using the perpendicular spacings, determine inter-well positions
    and use to get background values for each cell
    """
    axis_perp = np.array([-1 * axis[1], axis[0]])

    props = skmeas.regionprops(cells, coordinates="xy")
    all_points = np.array([prop.centroid for prop in props])
    all_pos = np.multiply(all_points, axis_perp).sum(axis=1)
    all_pos = np.sort(all_pos)
    all_nnseps = np.diff(all_pos)
    all_nnseps = np.abs(all_nnseps)
    all_nnseps = all_nnseps[all_nnseps > min_sep]
    # Get Median
    med_sep = np.median(all_nnseps)

    # Get mean of remaining seps with dist_width of median
    best_seps = (all_nnseps >= (med_sep - dist_width)) & (
        all_nnseps <= med_sep + dist_width
    )
    mean_sep = np.mean(all_nnseps[best_seps])
    half_mean_sep = mean_sep / 2
    shift = half_mean_sep * axis_perp
    # Use this sep to add in bg regions on either side of each well
    # Displace cells "left" and "right" by mean_sep/2
    bg_left = ndi.shift(cells, shift, order=0)
    bg_right = ndi.shift(cells, -shift, order=0)
    return [bg_left, bg_right]


def detect_out_of_focus_frames(data, mask=None, kvalue=3, output_folder=None):
    """
    Use standard focal strength measure to determine out of focus frames.
    Measure is relative, so need to process all frames; assumption is most
    are in focus.
    """
    if mask is None:
        mask = np.ones(data.shape[1:], dtype=bool)

    # ------------------------------
    # Compute focal strength
    # ------------------------------
    strength = np.zeros(data.shape[0])
    for tindex, frame in enumerate(data):
        frame = frame.astype(float)
        # frame = ndi.gaussian_filter(frame, 2.0)
        # frame /= np.median(frame)  # .mean()
        # frame -= frame.mean()
        # frame /= frame.std()
        # How many frame pixels are good edges
        # edges = skfilt.sobel(frame)[1:-1, 1:-1][mask[1:-1, 1:-1]]
        # thresh = skfilt.threshold_otsu(edges)
        # strength[tindex] = np.sum(edges >= thresh) / np.sum(mask[1:-1, 1:-1])

        # This is the Tenenbaum measure
        strength[tindex] = get_focal_strength_tenenbaum(frame, mask)
        # strength[tindex] = get_focal_strength_vollath(frame, mask)

    # ------------------------------
    # Perform basic outlier detection / thresholding
    # ------------------------------

    upper100 = np.array(
        [np.mean(np.sort(frame[mask], axis=None)[-100]) for frame in data]
    )
    strength /= upper100

    med = np.median(strength)
    mad = np.median(np.abs(strength - med))

    is_outlier = np.abs(strength - med) > (kvalue * mad)

    if output_folder is not None:
        plotting.plot_and_save_focal_strength(
            strength,
            median=med,
            threshold=kvalue * mad,
            twosided=True,
            filename=os.path.join(output_folder, "focal_strength.jpg"),
        )
        plotting.plot_and_save_focal_strength(
            np.array([frame[mask].mean() for frame in data]),
            filename=os.path.join(
                output_folder, "focal_strength_mean_intensity.jpg"
            ),
        )
        plotting.plot_and_save_focal_strength(
            strength / np.array([frame[mask].mean() for frame in data]),
            filename=os.path.join(
                output_folder, "focal_strength_divided_by_mean_intensity.jpg"
            ),
        )
        plotting.plot_and_save_focal_strength(
            strength / np.array([np.median(frame[mask]) for frame in data]),
            filename=os.path.join(
                output_folder, "focal_strength_divided_by_med_intensity.jpg"
            ),
        )

        upper100 = np.array(
            [np.mean(np.sort(frame[mask], axis=None)[-100]) for frame in data]
        )
        plotting.plot_and_save_focal_strength(
            strength / upper100,
            filename=os.path.join(
                output_folder, "focal_strength_divided_by_100_intensity.jpg"
            ),
        )
    return is_outlier


# ------------------------------
# Focal strength functions
# ------------------------------


def get_focal_strength_vollath(data, mask):
    data = data.astype(float)
    c1 = data[:-1] * data[1:]
    c2 = data[:-2] * data[2:]
    return np.sum(c1[mask[:-1]]) - np.sum(c2[mask[:-2]])


def get_focal_strength_tenenbaum_original(image, mask):
    return np.mean(
        skfilt.sobel(image.astype(float))[1:-1, 1:-1][mask[1:-1, 1:-1]]
    )


def get_focal_strength_tenenbaum(image, mask):
    edges = skfilt.sobel(
        # ndi.gaussian_filter(image.astype(float), 1.0)
        image.astype(float),
        # 1.0)
    )[1:-1, 1:-1][mask[1:-1, 1:-1]]
    strongest = np.sort(edges, axis=None)[-100:]
    return np.mean(strongest)


# simply copied from skimage.feature._canny


def smooth_with_function_and_mask(image, function, mask):
    """Smooth an image with a linear function, ignoring masked pixels

    Parameters
    ----------
    image : array
        Image you want to smooth.
    function : callable
        A function that does image smoothing.
    mask : array
        Mask with 1's for significant pixels, 0's for masked pixels.

    Notes
    ------
    This function calculates the fractional contribution of masked pixels
    by applying the function to the mask (which gets you the fraction of
    the pixel data that's due to significant points). We then mask the image
    and apply the function. The resulting values will be lower by the
    bleed-over fraction, so you can recalibrate by dividing by the function
    on the mask to recover the effect of smoothing from just the significant
    pixels.
    """
    bleed_over = function(mask.astype(float))
    masked_image = np.zeros(image.shape, image.dtype)
    masked_image[mask] = image[mask]
    smoothed_image = function(masked_image)
    output_image = smoothed_image / (bleed_over + np.finfo(float).eps)
    return output_image


# # Copied and modified


def modified_canny(image, sigma=1.0):
    """Edge filter an image using the Canny algorithm.

    MODIFIED BY JM: USE standard thresholding techniques to
    generate the thresholds

    Parameters
    -----------
    image : 2D array
        Greyscale input image to detect edges on; can be of any dtype.
    sigma : float
        Standard deviation of the Gaussian filter.
    low_threshold : float
        Lower bound for hysteresis thresholding (linking edges).
        If None, low_threshold is set to 10% of dtype's max.
    high_threshold : float
        Upper bound for hysteresis thresholding (linking edges).
        If None, high_threshold is set to 20% of dtype's max.
    mask : array, dtype=bool, optional
        Mask to limit the application of Canny to a certain area.
    use_quantiles : bool, optional
        If True then treat low_threshold and high_threshold as quantiles of the
        edge magnitude image, rather than absolute edge magnitude values.
        If True then the thresholds must be in the range [0, 1].

    Returns
    -------
    output : 2D array (image)
        The binary edge map.

    See also
    --------
    skimage.sobel

    Notes
    -----
    The steps of the algorithm are as follows:

    * Smooth the image using a Gaussian with ``sigma`` width.

    * Apply the horizontal and vertical Sobel operators to get the gradients
      within the image. The edge strength is the norm of the gradient.

    * Thin potential edges to 1-pixel wide curves. First, find the normal
      to the edge at each point. This is done by looking at the
      signs and the relative magnitude of the X-Sobel and Y-Sobel
      to sort the points into 4 categories: horizontal, vertical,
      diagonal and antidiagonal. Then look in the normal and reverse
      directions to see if the values in either of those directions are
      greater than the point in question. Use interpolation to get a mix of
      points instead of picking the one that's the closest to the normal.

    * Perform a hysteresis thresholding: first label all points above the
      high threshold as edges. Then recursively label any point above the
      low threshold that is 8-connected to a labeled point as an edge.

    References
    -----------
    Canny, J., A Computational Approach To Edge Detection, IEEE Trans.
    Pattern Analysis and Machine Intelligence, 8:679-714, 1986

    William Green's Canny tutorial
    http://dasl.mem.drexel.edu/alumni/bGreen/www.pages.drexel.edu/_weg22/can_tut.html

    Examples
    --------
    >>> from skimage import feature
    >>> # Generate noisy image of a square
    >>> im = np.zeros((256, 256))
    >>> im[64:-64, 64:-64] = 1
    >>> im += 0.2 * np.random.rand(*im.shape)
    >>> # First trial with the Canny filter, with the default smoothing
    >>> edges1 = feature.canny(im)
    >>> # Increase the smoothing for better results
    >>> edges2 = feature.canny(im, sigma=3)
    """

    #
    # The steps involved:
    #
    # * Smooth using the Gaussian with sigma above.
    #
    # * Apply the horizontal and vertical Sobel operators to get the gradients
    #   within the image. The edge strength is the sum of the magnitudes
    #   of the gradients in each direction.
    #
    # * Find the normal to the edge at each point using the arctangent of the
    #   ratio of the Y sobel over the X sobel - pragmatically, we can
    #   look at the signs of X and Y and the relative magnitude of X vs Y
    #   to sort the points into 4 categories: horizontal, vertical,
    #   diagonal and antidiagonal.
    #
    # * Look in the normal and reverse directions to see if the values
    #   in either of those directions are greater than the point in question.
    #   Use interpolation to get a mix of points instead of picking the one
    #   that's the closest to the normal.
    #
    # * Label all points above the high threshold as edges.
    # * Recursively label any point above the low threshold that is 8-connected
    #   to a labeled point as an edge.
    #
    # Regarding masks, any point touching a masked point will have a gradient
    # that is "infected" by the masked point, so it's enough to erode the
    # mask by one and then mask the output. We also mask out the border points
    # because who knows what lies beyond the edge of the image?
    #
    mask = np.ones(image.shape, dtype=bool)

    def fsmooth(x):
        return ndi.gaussian_filter(x, sigma, mode="constant")

    smoothed = smooth_with_function_and_mask(image, fsmooth, mask)
    jsobel = ndi.sobel(smoothed, axis=1)
    isobel = ndi.sobel(smoothed, axis=0)
    abs_isobel = np.abs(isobel)
    abs_jsobel = np.abs(jsobel)
    magnitude = np.hypot(isobel, jsobel)

    #
    # Make the eroded mask. Setting the border value to zero will wipe
    # out the image edges for us.
    #
    s = ndi.generate_binary_structure(2, 2)
    eroded_mask = ndi.binary_erosion(mask, s, border_value=0)
    eroded_mask = eroded_mask & (magnitude > 0)
    #
    # --------- Find local maxima --------------
    #
    # Assign each point to have a normal of 0-45 degrees, 45-90 degrees,
    # 90-135 degrees and 135-180 degrees.
    #
    local_maxima = np.zeros(image.shape, bool)
    # ----- 0 to 45 degrees ------
    pts_plus = (isobel >= 0) & (jsobel >= 0) & (abs_isobel >= abs_jsobel)
    pts_minus = (isobel <= 0) & (jsobel <= 0) & (abs_isobel >= abs_jsobel)
    pts = pts_plus | pts_minus
    pts = eroded_mask & pts
    # Get the magnitudes shifted left to make a matrix of the points to the
    # right of pts. Similarly, shift left and down to get the points to the
    # top right of pts.
    c1 = magnitude[1:, :][pts[:-1, :]]
    c2 = magnitude[1:, 1:][pts[:-1, :-1]]
    m = magnitude[pts]
    w = abs_jsobel[pts] / abs_isobel[pts]
    c_plus = c2 * w + c1 * (1 - w) <= m
    c1 = magnitude[:-1, :][pts[1:, :]]
    c2 = magnitude[:-1, :-1][pts[1:, 1:]]
    c_minus = c2 * w + c1 * (1 - w) <= m
    local_maxima[pts] = c_plus & c_minus
    # ----- 45 to 90 degrees ------
    # Mix diagonal and vertical
    #
    pts_plus = (isobel >= 0) & (jsobel >= 0) & (abs_isobel <= abs_jsobel)
    pts_minus = (isobel <= 0) & (jsobel <= 0) & (abs_isobel <= abs_jsobel)
    pts = pts_plus | pts_minus
    pts = eroded_mask & pts
    c1 = magnitude[:, 1:][pts[:, :-1]]
    c2 = magnitude[1:, 1:][pts[:-1, :-1]]
    m = magnitude[pts]
    w = abs_isobel[pts] / abs_jsobel[pts]
    c_plus = c2 * w + c1 * (1 - w) <= m
    c1 = magnitude[:, :-1][pts[:, 1:]]
    c2 = magnitude[:-1, :-1][pts[1:, 1:]]
    c_minus = c2 * w + c1 * (1 - w) <= m
    local_maxima[pts] = c_plus & c_minus
    # ----- 90 to 135 degrees ------
    # Mix anti-diagonal and vertical
    #
    pts_plus = (isobel <= 0) & (jsobel >= 0) & (abs_isobel <= abs_jsobel)
    pts_minus = (isobel >= 0) & (jsobel <= 0) & (abs_isobel <= abs_jsobel)
    pts = pts_plus | pts_minus
    pts = eroded_mask & pts
    c1a = magnitude[:, 1:][pts[:, :-1]]
    c2a = magnitude[:-1, 1:][pts[1:, :-1]]
    m = magnitude[pts]
    w = abs_isobel[pts] / abs_jsobel[pts]
    c_plus = c2a * w + c1a * (1.0 - w) <= m
    c1 = magnitude[:, :-1][pts[:, 1:]]
    c2 = magnitude[1:, :-1][pts[:-1, 1:]]
    c_minus = c2 * w + c1 * (1.0 - w) <= m
    local_maxima[pts] = c_plus & c_minus
    # ----- 135 to 180 degrees ------
    # Mix anti-diagonal and anti-horizontal
    #
    pts_plus = (isobel <= 0) & (jsobel >= 0) & (abs_isobel >= abs_jsobel)
    pts_minus = (isobel >= 0) & (jsobel <= 0) & (abs_isobel >= abs_jsobel)
    pts = pts_plus | pts_minus
    pts = eroded_mask & pts
    c1 = magnitude[:-1, :][pts[1:, :]]
    c2 = magnitude[:-1, 1:][pts[1:, :-1]]
    m = magnitude[pts]
    w = abs_jsobel[pts] / abs_isobel[pts]
    c_plus = c2 * w + c1 * (1 - w) <= m
    c1 = magnitude[1:, :][pts[:-1, :]]
    c2 = magnitude[1:, :-1][pts[:-1, 1:]]
    c_minus = c2 * w + c1 * (1 - w) <= m
    local_maxima[pts] = c_plus & c_minus

    # Determine thresholds....

    high_threshold = skfilt.threshold_li(magnitude)
    low_threshold = skfilt.threshold_li(magnitude[magnitude <= high_threshold])

    #
    # ---- Create two masks at the two thresholds.
    #
    high_mask = local_maxima & (magnitude >= high_threshold)
    low_mask = local_maxima & (magnitude >= low_threshold)
    #
    # Segment the low-mask, then only keep low-segments that have
    # some high_mask component in them
    #
    strel = np.ones((3, 3), bool)
    labels, count = ndi.label(low_mask, strel)
    if count == 0:
        return low_mask

    sums = np.array(
        ndi.sum(high_mask, labels, np.arange(count, dtype=np.int32) + 1),
        copy=False,
        ndmin=1,
    )
    good_label = np.zeros((count + 1,), bool)
    good_label[1:] = sums > 0
    output_mask = good_label[labels]
    return output_mask
