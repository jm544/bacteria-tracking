"""app.py

Graphical User Interface Application

For now, use a web application

J. Metz <metz.jp@gmail.com>
"""

import sqlite3
from flask import (
    Flask,
    render_template,
    redirect,
    url_for,
    g,  # A "global" thingy
    jsonify,
)
import os
from bactrack import dataio as btio, detection as btdetect, tracking as bttrack

# Base64 encoded files
import io
from PIL import Image
import base64

# Cells to polygons
import numpy as np
import skimage.measure as skmeas
import json

PWD = os.path.abspath(os.path.dirname(__file__))
DATABASE = os.path.join(PWD, "db.sqlite")

# =========================================================
# Main external functions
# =========================================================


def run_ui(
    filenames=[], debug=False, dev=False, skip_timepoints=(), reverse=False
):
    app = create_app(
        filenames=filenames,
        skip_timepoints=skip_timepoints,
        dev=dev,
        reverse=reverse,
    )
    print("WARNING: NOT ALL KWARGS IMPLEMENTED BELOW..??")
    app.run(debug=debug)


# =========================================================
# Framework specific
# =========================================================

# ------------------------------
# sqlite3 database
# ------------------------------


def get_db():
    db = getattr(g, "_database", None)
    if db is None:
        db = g._database = sqlite3.connect(DATABASE)

    # Make queries return dictionaries instead of tuples
    # def make_dicts(cursor, row):
    #   return dict(
    #        (cursor.description[idx][0], value)
    #        for idx, value in enumerate(row)
    #    )
    # db.row_factory = make_dicts
    # Even easier - sqlite3.Row is a named tuple
    db.row_factory = sqlite3.Row

    return db


def init_db(filenames):
    db = get_db()
    cur = db.cursor()
    cur.execute("DROP TABLE IF EXISTS files")
    cur.execute("CREATE TABLE files (date timestamp, filename text)")
    cur.execute("DROP TABLE IF EXISTS cells")
    cur.execute("CREATE TABLE cells (framenumber int, jsonpoly text)")
    if filenames:
        cur.executemany(
            "INSERT INTO files VALUES (?,?)",
            [(os.path.getmtime(f), f) for f in filenames],
        )
    db.commit()


def query_db(query, args=(), one=False):
    cur = get_db().execute(query, args)
    rv = cur.fetchall()
    cur.close()
    return (rv[0] if rv else None) if one else rv


def enter_cells_into_db(cells):
    """
    Cells is a list of label images (at present).
    For each label image, extract the contour for each
    cell and store as a list of polygons in the database.
    For this we will use json.dumps.
    """
    inserts = []
    for cellim in cells:
        # polys will be a dict: label : polygon (array of points)
        polys = cell_image_to_polygons(cellim)
        # Convert arrays to lists for serialization
        polys = {key: poly.tolist() for key, poly in polys.items()}
        inserts.append(json.dumps(polys))
    db = get_db()
    cur = db.cursor()
    cur.execute("DROP TABLE IF EXISTS cells")
    cur.execute("CREATE TABLE cells (framenumber int, jsonpoly text)")
    cur.executemany(
        "INSERT INTO cells VALUES (?,?)",
        [(tind, insert) for tind, insert in enumerate(inserts)],
    )
    db.commit()


# ------------------------------
# Flask application
# ------------------------------


def create_app(filenames=(), skip_timepoints=(), dev=False, reverse=False):
    app = Flask(__name__)

    if skip_timepoints:
        print("IGNORING SKIP_TIMEPOINTS IN CREATE_APP")

    if dev:
        print("IGNORING DEV IN CREATE_APP")

    if reverse:
        print("IGNORING REVERSE IN CREATE_APP")

    # do we need a with app.app_context(): here?
    # Yup! =)
    with app.app_context():
        init_db(filenames)

    @app.route("/")
    def main():
        files = query_db("select * from files")
        images = []
        for im in btio.load_data([f["filename"] for f in files]):
            im = im.astype(float)
            im -= im.min()
            im /= im.max()
            im *= 255
            im = im.astype("uint8")
            imstr = image_to_base64_encoded_dataurl(im, format="jpeg")
            images.append(imstr)
        cells = [
            {
                "framenumber": cell["framenumber"],
                "cells": json.loads(cell["jsonpoly"]),
            }
            for cell in query_db("select * from cells")
        ]
        return render_template(
            "index.html", files=files, images=images, cells=cells
        )

    @app.route("/process_dataset")
    def process_dataset():
        print("Processing the dataset...", flush=True)
        files = query_db("select * from files")
        data = btio.load_data([f["filename"] for f in files])
        print("  Detecting cells...", flush=True)
        cells0 = btdetect.detect_cells(data)
        print("  Tracking cells...", flush=True)
        cells = bttrack.track_cells(cells0, data)
        # Put back into the database
        print("  Adding cells to database...", flush=True)
        enter_cells_into_db(cells)
        cells = [
            {
                "framenumber": cell["framenumber"],
                "cells": json.loads(cell["jsonpoly"]),
            }
            for cell in query_db("select * from cells")
        ]
        return jsonify(cells)

    @app.teardown_appcontext
    def close_connection(exception):
        db = getattr(g, "_database", None)
        if db is not None:
            db.close()

    return app


def create_embedded_output_visualisation(data, cells, stats):
    # 1: output images
    ims = "var frames = ["
    for im in data:
        im = im.astype(float)
        im -= im.min()
        im /= im.max()
        im *= 255
        im = im.astype("uint8")
        imstr = image_to_base64_encoded_dataurl(im, format="jpeg")
        # fout.write('<div><img src="%s"/></div>'%imstr)
        ims += 'PIXI.Texture.from("%s"),' % imstr
    ims += "];\n"
    fout = open("test.html", "w")

    # 2: Add cell outlines
    celloutlines = "var celloutlines = [];\n"

    # 3: Get the stats plot

    outtxt = viewer_template % (ims, celloutlines)
    fout.write(outtxt)
    fout.close()
    sys.exit()


# =========================================================
# Utility functions for the web visualisation
# =========================================================


def image_to_base64_encoded_dataurl(arr, format="jpeg"):
    out = io.BytesIO()
    im = Image.fromarray(arr).convert("RGB")
    im.save(out, format=format)
    dataurl = "data:image/%s;base64,%s" % (
        format,
        base64.b64encode(out.getvalue()).decode("ascii"),
    )
    return dataurl


def cell_image_to_polygons(cellim):
    """
    Takes a label image and returns polygons representing the labels,
    as a dictionary
    """
    polys = {}
    print("Converting cell image to contours..")
    for l in range(1, cellim.max() + 1):
        bw = cellim == l
        if not np.any(bw):
            continue
        poly = skmeas.find_contours(bw, 0.5)
        # Approximate - don't need to have lines between each pixel
        poly = skmeas.approximate_polygon(poly[0], 1.5)
        polys[l] = poly
    return polys
