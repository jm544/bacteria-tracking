"""qt_widgets.py

Custom QT bits and bobs

J. Metz <metz.jp@gmail.com>
"""

from PyQt5 import QtWidgets as qw
import skimage.viewer as skview
from skimage.viewer.plugins.lineprofile import LineProfile
from skimage.viewer.qt import QtWidgets, QtGui, Qt, Signal

from bactrack import dataio as btio


def get_filenames_gui():
    app = qw.QApplication([])
    filename = qw.QFileDialog.getOpenFileName(
        parent=None,
        caption="Select data file"
        # filter='*.tif',
    )
    return [filename[0]]


def select_filenames_and_optionally_select_frames_gui():
    filenames = get_filenames_gui()

    data = btio.load_data(filenames)
    data = data.astype(float) / data.max()

    viewer = SelectableCollectionViewer(data)
    viewer.show()
    removed_frames = [
        ind for ind, val in enumerate(viewer.frame_removed) if val
    ]
    return filenames, removed_frames


class SelectableCollectionViewer(skview.CollectionViewer):
    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.frame_removed = [False for _ in range(self.num_images)]
        self.draw_cross()

    def update_index(self, name, index):
        # First call super
        # Now, if we have been removed, show that
        super().update_index(name, index)
        self.set_cross_visibility()

    def draw_cross(self):
        x0, x1 = self.ax.get_xlim()
        y0, y1 = self.ax.get_ylim()
        self.cross = [
            self.ax.plot(
                [x0, x1],
                [y0, y1],
                "r-",
                lw=20,
                alpha=0.3,
                scalex=False,
                scaley=False,
            )[0],
            self.ax.plot(
                [x0, x1],
                [y1, y0],
                "r-",
                lw=20,
                alpha=0.3,
                scalex=False,
                scaley=False,
            )[0],
        ]
        self.set_cross_visibility()

    def set_cross_visibility(self):
        [
            line.set_visible(self.frame_removed[self.index])
            for line in self.cross
        ]
        self.fig.canvas.draw()
        self.redraw()

    def keyPressEvent(self, event):
        if type(event) == QtGui.QKeyEvent:
            key = event.key()
            # Number keys (code: 0 = key 48, 9 = key 57) move to deciles
            if 48 <= key < 58:
                index = int(0.1 * (key - 48) * self.num_images)
                self.update_index("", index)
                event.accept()
            elif key == Qt.Key_Left:
                index = (self.index - 1) % self.num_images
                self.update_index("", index)
                event.accept()
            elif key == Qt.Key_Right:
                index = (self.index + 1) % self.num_images
                self.update_index("", index)
                event.accept()
            elif key == Qt.Key_Space:
                self.frame_removed[self.index] = ~self.frame_removed[
                    self.index
                ]
                self.set_cross_visibility()
                event.accept()
            else:
                event.ignore()
        else:
            event.ignore()
