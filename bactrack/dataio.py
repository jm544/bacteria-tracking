"""io.py

IO related functions for bactrack

J. Metz <metz.jp@gmail.com>
"""

import os
import copy
import glob
import skimage.io as skio
import pandas as pd
import pickle


# Specific to polygon selector...
import numpy as np

from matplotlib.widgets import _SelectorWidget, Line2D, ToolHandles
from matplotlib.path import Path
import matplotlib.pyplot as plt


# =========================================================
# IO
# =========================================================


def load_data(filename, tmax=None):
    if isinstance(filename, (list, tuple)):
        if len(filename) == 1:
            return skio.imread(filename[0])[:tmax].astype(float)
        else:
            return [
                skio.imread(fname).astype(float) for fname in filename[:tmax]
            ]
    else:
        return skio.imread(filename)[:tmax].astype(float)


def save_stack(stack_data, filename):
    """
    Simple wrapper around skimage.io to save the stack
    """
    skio.imsave(filename, np.array(stack_data), imagej=True, compress=9)


def save_stats(stats, filename, channel_stats=None, skip=["lifetime", "time"]):
    stats = copy.deepcopy(stats)
    print("Saving stats...")
    basename, ext = os.path.splitext(filename)

    # ------------------------------
    # Just important bit
    # ------------------------------
    dataframes = []
    if channel_stats is not None:
        timevals = channel_stats["time"]
        for statname, values in sorted(channel_stats.items()):
            if statname is "time":
                continue
            dataframes.append(
                pd.DataFrame({f"Channel {statname}": values}, index=timevals)
            )

    for trajid, statobj in sorted(stats.items()):
        statname = "dcc mean / dcc bg"
        timevals = statobj["time"]
        statvalue = statobj[statname]
        dataframes.append(
            pd.DataFrame({f"{trajid} {statname}": statvalue}, index=timevals)
        )
    df = pd.concat(dataframes, ignore_index=False, join="outer", axis=1)
    df.to_csv(filename)

    # ------------------------------
    # Save to pickle for python loading

    filename_pkl = "{}.pkl".format(os.path.splitext(filename)[0])
    with open(filename_pkl, "wb") as fpkl:
        pickle.dump({"cells": stats, "channel": channel_stats}, fpkl)

    # ------------------------------
    # Full version
    # ------------------------------
    dataframes = []
    if channel_stats is not None:
        timevals = channel_stats["time"]
        for statname, values in sorted(channel_stats.items()):
            if statname in skip:
                continue
            dataframes.append(
                pd.DataFrame({f"Channel {statname}": values}, index=timevals)
            )
    for trajid, statobj in sorted(stats.items()):
        timevals = statobj["time"]
        for statname, statvalue in sorted(statobj.items()):
            if statname in skip:
                continue
            dataframes.append(
                pd.DataFrame(
                    {f"{trajid} {statname}": statvalue}, index=timevals
                )
            )
    df = pd.concat(dataframes, ignore_index=False, join="outer", axis=1)

    df.to_csv(f"{basename}_full{ext}")

    with open(f"{basename}_full.pkl", "wb") as fout_pickle:
        pickle.dump(stats, fout_pickle)


def get_mask(data, filename=None, save=False, load=False):
    if isinstance(filename, (list, tuple)):
        filename = filename[0]

    if filename:
        mask_filename = "%s_mask.png" % filename
    if load and filename:
        # Look for mask in predefined place
        if os.path.isfile(mask_filename):
            print("Loading mask from file")
            mask = skio.imread(mask_filename) >= 1
            return mask

    plt.switch_backend("qt5agg")
    fig, ax = plt.subplots()
    im = ax.imshow(data[0])
    selector = SelectFromImage(ax, im)

    print("Select region in the figure by enclosing within a polygon.")
    print("Press the 'esc' key to start a new polygon.")
    print("Try holding the 'shift' key to move all of the vertices.")
    print("Try holding the 'ctrl' key to move a single vertex.")

    plt.show()
    selector.disconnect()

    if save and filename:
        print("Saving mask")
        skio.imsave(mask_filename, selector.mask)

    return selector.mask


# =========================================================
# Polygon ROI
# =========================================================


class PolygonSelector(_SelectorWidget):
    """Select a polygon region of an axes.

    Place vertices with each mouse click, and make the selection by completing
    the polygon (clicking on the first vertex). Hold the *ctrl* key and click
    and drag a vertex to reposition it (the *ctrl* key is not necessary if the
    polygon has already been completed). Hold the *shift* key and click and
    drag anywhere in the axes to move all vertices. Press the *esc* key to
    start a new polygon.

    For the selector to remain responsive you must keep a reference to
    it.

    Parameters
    ----------
    ax : :class:`~matplotlib.axes.Axes`
        The parent axes for the widget.
    onselect : function
        When a polygon is completed or modified after completion,
        the `onselect` function is called and passed a list of the vertices as
        ``(xdata, ydata)`` tuples.
    useblit : bool, optional
    lineprops : dict, optional
        The line for the sides of the polygon is drawn with the properties
        given by `lineprops`. The default is ``dict(color='k', linestyle='-',
        linewidth=2, alpha=0.5)``.
    markerprops : dict, optional
        The markers for the vertices of the polygon are drawn with the
        properties given by `markerprops`. The default is ``dict(marker='o',
        markersize=7, mec='k', mfc='k', alpha=0.5)``.
    vertex_select_radius : float, optional
        A vertex is selected (to complete the polygon or to move a vertex)
        if the mouse click is within `vertex_select_radius` pixels of the
        vertex. The default radius is 15 pixels.

    See Also
    --------
    :ref:`sphx_glr_gallery_widgets_polygon_selector_demo.py`
    """

    def __init__(
        self,
        ax,
        onselect,
        useblit=False,
        lineprops=None,
        markerprops=None,
        vertex_select_radius=15,
    ):
        # The state modifiers 'move', 'square', and 'center' are expected by
        # _SelectorWidget but are not supported by PolygonSelector
        # Note: could not use the existing 'move' state modifier in-place of
        # 'move_all' because _SelectorWidget automatically discards 'move'
        # from the state on button release.
        state_modifier_keys = dict(
            clear="escape",
            move_vertex="control",
            move_all="shift",
            move="not-applicable",
            square="not-applicable",
            center="not-applicable",
        )
        _SelectorWidget.__init__(
            self,
            ax,
            onselect,
            useblit=useblit,
            state_modifier_keys=state_modifier_keys,
        )

        self._xs, self._ys = [0], [0]
        self._polygon_completed = False

        if lineprops is None:
            lineprops = dict(color="k", linestyle="-", linewidth=2, alpha=0.5)
        lineprops["animated"] = self.useblit
        self.line = Line2D(self._xs, self._ys, **lineprops)
        self.ax.add_line(self.line)

        if markerprops is None:
            markerprops = dict(mec="k", mfc=lineprops.get("color", "k"))
        self._polygon_handles = ToolHandles(
            self.ax,
            self._xs,
            self._ys,
            useblit=self.useblit,
            marker_props=markerprops,
        )

        self._active_handle_idx = -1
        self.vertex_select_radius = vertex_select_radius

        self.artists = [self.line, self._polygon_handles.artist]
        self.set_visible(True)

    def _press(self, event):
        """Button press event handler"""
        # Check for selection of a tool handle.
        if (self._polygon_completed or "move_vertex" in self.state) and len(
            self._xs
        ) > 0:
            h_idx, h_dist = self._polygon_handles.closest(event.x, event.y)
            if h_dist < self.vertex_select_radius:
                self._active_handle_idx = h_idx
        # Save the vertex positions at the time of the press event (needed to
        # support the 'move_all' state modifier).
        self._xs_at_press, self._ys_at_press = self._xs[:], self._ys[:]

    def _release(self, event):
        """Button release event handler"""
        # Release active tool handle.
        if self._active_handle_idx >= 0:
            self._active_handle_idx = -1

        # Complete the polygon.
        elif (
            len(self._xs) > 3
            and self._xs[-1] == self._xs[0]
            and self._ys[-1] == self._ys[0]
        ):
            self._polygon_completed = True

        # Place new vertex.
        elif (
            not self._polygon_completed
            and "move_all" not in self.state
            and "move_vertex" not in self.state
        ):
            self._xs.insert(-1, event.xdata)
            self._ys.insert(-1, event.ydata)

        if self._polygon_completed:
            self.onselect(self.verts)

    def onmove(self, event):
        """Cursor move event handler and validator"""
        # Method overrides _SelectorWidget.onmove because the polygon selector
        # needs to process the move callback even if there is no button press.
        # _SelectorWidget.onmove include logic to ignore move event if
        # eventpress is None.
        if not self.ignore(event):
            event = self._clean_event(event)
            self._onmove(event)
            return True
        return False

    def _onmove(self, event):
        """Cursor move event handler"""
        # Move the active vertex (ToolHandle).
        if self._active_handle_idx >= 0:
            idx = self._active_handle_idx
            self._xs[idx], self._ys[idx] = event.xdata, event.ydata
            # Also update the end of the polygon line if the first vertex is
            # the active handle and the polygon is completed.
            if idx == 0 and self._polygon_completed:
                self._xs[-1], self._ys[-1] = event.xdata, event.ydata

        # Move all vertices.
        elif "move_all" in self.state and self.eventpress:
            dx = event.xdata - self.eventpress.xdata
            dy = event.ydata - self.eventpress.ydata
            for k in range(len(self._xs)):
                self._xs[k] = self._xs_at_press[k] + dx
                self._ys[k] = self._ys_at_press[k] + dy

        # Do nothing if completed or waiting for a move.
        elif (
            self._polygon_completed
            or "move_vertex" in self.state
            or "move_all" in self.state
        ):
            return

        # Position pending vertex.
        else:
            # Calculate distance to the start vertex.
            x0, y0 = self.line.get_transform().transform(
                (self._xs[0], self._ys[0])
            )
            v0_dist = np.sqrt((x0 - event.x) ** 2 + (y0 - event.y) ** 2)
            # Lock on to the start vertex if near it and ready to complete.
            if len(self._xs) > 3 and v0_dist < self.vertex_select_radius:
                self._xs[-1], self._ys[-1] = self._xs[0], self._ys[0]
            else:
                self._xs[-1], self._ys[-1] = event.xdata, event.ydata

        self._draw_polygon()

    def _on_key_press(self, event):
        """Key press event handler"""
        # Remove the pending vertex if entering the 'move_vertex' or
        # 'move_all' mode
        if not self._polygon_completed and (
            "move_vertex" in self.state or "move_all" in self.state
        ):
            self._xs, self._ys = self._xs[:-1], self._ys[:-1]
            self._draw_polygon()

    def _on_key_release(self, event):
        """Key release event handler"""
        # Add back the pending vertex if leaving the 'move_vertex' or
        # 'move_all' mode (by checking the released key)
        if not self._polygon_completed and (
            event.key == self.state_modifier_keys.get("move_vertex")
            or event.key == self.state_modifier_keys.get("move_all")
        ):
            self._xs.append(event.xdata)
            self._ys.append(event.ydata)
            self._draw_polygon()
        # Reset the polygon if the released key is the 'clear' key.
        elif event.key == self.state_modifier_keys.get("clear"):
            event = self._clean_event(event)
            self._xs, self._ys = [event.xdata], [event.ydata]
            self._polygon_completed = False
            self.set_visible(True)

    def _draw_polygon(self):
        """Redraw the polygon based on the new vertex positions."""
        self.line.set_data(self._xs, self._ys)
        # Only show one tool handle at the start and end vertex of the polygon
        # if the polygon is completed or the user is locked on to the start
        # vertex.
        if self._polygon_completed or (
            len(self._xs) > 3
            and self._xs[-1] == self._xs[0]
            and self._ys[-1] == self._ys[0]
        ):
            self._polygon_handles.set_data(self._xs[:-1], self._ys[:-1])
        else:
            self._polygon_handles.set_data(self._xs, self._ys)
        self.update()

    @property
    def verts(self):
        """Get the polygon vertices.

        Returns
        -------
        list
            A list of the vertices of the polygon as ``(xdata, ydata)`` tuples.
        """
        return list(zip(self._xs[:-1], self._ys[:-1]))


class SelectFromImage(object):
    """Select mask for image using `PolygonSelector`.

    Parameters
    ----------
    ax : :class:`~matplotlib.axes.Axes`
        Axes to interact with.

    image:

    alpha_other : 0 <= float <= 1
        To highlight a selection, this tool sets all selected points to an
        alpha value of 1 and non-selected points to `alpha_other`.
    """

    def __init__(self, ax, image, alpha_other=0.3):
        self.canvas = ax.figure.canvas
        self.image = image
        self.alpha_other = alpha_other

        self.mask = np.ones(image.get_size(), dtype=bool)
        self.maskim = ax.imshow(
            self.mask, alpha=0.3, cmap="jet", vmin=0, vmax=1
        )
        self.poly = PolygonSelector(ax, self.onselect)
        self.xys = np.array(self.mask.nonzero()).T
        self.xys = self.xys[:, ::-1]

    def onselect(self, verts):
        path = Path(verts)
        self.ind = np.nonzero(path.contains_points(self.xys))[0]
        self.mask[:] = False
        self.mask[np.unravel_index(self.ind, self.mask.shape)] = True
        self.maskim.set_data(self.mask)
        # self.fc[:, -1] = self.alpha_other
        # self.fc[self.ind, -1] = 1
        # self.collection.set_facecolors(self.fc)
        self.canvas.draw_idle()

    def disconnect(self):
        self.poly.disconnect_events()
        # self.fc[:, -1] = 1
        # self.collection.set_facecolors(self.fc)
        self.canvas.draw_idle()
