"""io.py

Development related functions for bactrack

J. Metz <metz.jp@gmail.com>
"""

import os
import glob

PWD = os.path.abspath(os.path.dirname(__file__))
ROOT = os.path.abspath(os.path.join(PWD, ".."))

# =========================================================
# Testing
# =========================================================


def get_test_data_region2(
    datapath=os.path.join(ROOT, "testdata2"), filepattern="*.tif"
):
    pattern = os.path.join(datapath, filepattern)
    # Should be a single tif file
    files = glob.glob(pattern)
    if len(files) != 1:
        raise Exception("1 data file expected in %s" % datapath)
    return files[0]


def get_test_data_filenames(
    datapath=os.path.join(ROOT, "testdata"), filepattern="*.png"
):
    pattern = os.path.join(datapath, filepattern)

    return sorted(glob.glob(pattern))


def get_test_data_region0_filenames(
    datapath=os.path.join(ROOT, "testdata_region0"), filepattern="*.png"
):
    pattern = os.path.join(datapath, filepattern)

    return sorted(glob.glob(pattern))
