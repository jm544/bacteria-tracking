"""plotting.py

Plotting

J. Metz <metz.jp@gmail.com>
"""
import os
import copy
import tqdm
import numpy as np
import matplotlib.pyplot as plt
import scipy.ndimage as ndi
import skimage.measure as skmeas
import skimage.morphology as skmo
from PIL import Image, ImageDraw
from bactrack import utility


def output_cell_images_mpl(
    data, cells, outputbase, vmin=None, vmax=None, text=False, taxis=None
):
    vals = np.linspace(0, 1, 100000)
    if taxis is None:
        taxis = np.arange(len(data))
    np.random.shuffle(vals)
    cmap_rand = plt.cm.colors.ListedColormap(plt.cm.jet(vals))
    print("Plotting cell images [mpl]...")
    for tpoint, frame, cell in tqdm.tqdm(zip(taxis, data, cells)):
        # filename = "%s_detection_%0.6d.png"%(outputbase, t)
        filename = "%s_detection_%0.6d.jpg" % (outputbase, tpoint)
        output_cell_image_mpl(
            frame, cell, cmap_rand, vmin, vmax, text, filename
        )


def output_cell_image_mpl(image, cells, cmap, vmin, vmax, text, filename):
    fig = plt.figure(figsize=(12, 12 * image.shape[0] / image.shape[1]))
    ax = fig.add_subplot(111)
    plt.imshow(image, cmap="gray")
    plt.imshow(
        np.ma.masked_where(cells == 0, cells),
        alpha=0.4,
        cmap=cmap,
        vmin=vmin,
        vmax=vmax,
    )
    if text:
        for p in skmeas.regionprops(cells):
            plt.text(p.centroid[1], p.centroid[0], str(p.label))

    # N = c.max()
    # for label in range(1, N+1):
    #    color=plt.cm.jet(label/N)
    #    plt.contour((c==label)>0, levels=[0.5], colors=[color])
    ax.set_position([0, 0, 1, 1])
    plt.savefig(filename)
    plt.close()


def output_cell_images_skimage(
    data, cells, outputbase, vmin=None, vmax=None, text=False, taxis=None
):
    vals = np.linspace(0, 1, 100000)
    if taxis is None:
        taxis = np.arange(len(data))
    np.random.shuffle(vals)
    cmap_rand = plt.cm.colors.ListedColormap(plt.cm.jet(vals))
    print("Plotting cell images [skimage]...")
    for tpoint, frame, cell in tqdm.tqdm(zip(taxis, data, cells)):
        fig = plt.figure(figsize=(12, 12 * d.shape[0] / d.shape[1]))
        ax = fig.add_subplot(111)
        plt.imshow(d, cmap="gray")
        plt.imshow(
            np.ma.masked_where(c == 0, c),
            alpha=0.4,
            cmap=cmap_rand,
            vmin=vmin,
            vmax=vmax,
        )
        if text:
            for p in skmeas.regionprops(c):
                plt.text(p.centroid[1], p.centroid[0], str(p.label))

        # N = c.max()
        # for label in range(1, N+1):
        #    color=plt.cm.jet(label/N)
        #    plt.contour((c==label)>0, levels=[0.5], colors=[color])
        ax.set_position([0, 0, 1, 1])
        # plt.savefig("%s_detection_%0.6d.png"%(outputbase, t))
        plt.savefig("%s_detection_%0.6d.jpg" % (outputbase, tpoint))
        plt.close()


def output_cell_images_pil(
    data,
    cells,
    outputbase,
    vmin=None,
    vmax=None,
    text=False,
    wells=None,
    wells0=None,
    taxis=None,
):
    num = 100000
    if taxis is None:
        taxis = np.arange(len(data))
    colours = plt.cm.jet(np.arange(num + 1) / num)[:, :3]
    np.random.shuffle(colours)
    if wells is None:
        wells = [None for i in range(len(data))]
    if wells0 is None:
        wells0 = [None for i in range(len(data))]

    print("Plotting cell images [PIL]...")
    for tpoint, frame, cell, well, well0 in tqdm.tqdm(
        zip(taxis, data, cells, wells, wells0)
    ):
        # filename = "%s_detection_%0.6d.png"%(outputbase, t)
        filename = "%s_detection_%0.6d.jpg" % (outputbase, tpoint)
        output_cell_image_pil(
            frame,
            cell,
            colours,
            filename,
            text=text,
            well_mask=well,
            well_mask0=well0,
        )


def output_cell_image_pil(
    image,
    label_in,
    colours,
    filename,
    text=False,
    well_mask=None,
    well_mask0=None,
):
    label = label_in.copy()
    label[ndi.binary_erosion(label > 0, structure=skmo.disk(2))] = 0
    image = image.copy() - image.min()
    image /= image.max()
    img_rgb = np.tile(image[..., None], (1, 1, 3))

    # Can't use this
    label_rgb = colours[label]

    out_img = img_rgb.copy()
    out_img[label > 0, :] = (
        0.5 * out_img[label > 0, :] + 0.5 * label_rgb[label > 0, :]
    )
    out_img = (255 * out_img).astype("uint8")

    if well_mask is not None:
        well_mask[
            ndi.binary_erosion(well_mask > 0, structure=skmo.disk(2))
        ] = 0
        out_img[well_mask > 0, :] = np.array([0, 255, 0])

    if well_mask0 is not None:
        well_mask0 = (
            ndi.binary_dilation(well_mask0 > 0, structure=skmo.disk(2))
            ^ well_mask0
        )
        out_img[well_mask0 > 0, :] = np.array([0, 255, 255])

    im = Image.fromarray(out_img)
    draw = ImageDraw.Draw(im)

    if text:
        for prop in skmeas.regionprops(label_in):
            draw.text(
                prop.centroid[::-1],
                str(prop.label),
                color=(255, 255, 255, 128),
            )

    # skio.imsave(filename, np.array(im))
    im.save(filename)


def plot_stats(*args, backend="mpl", **kwargs):
    print("Plotting stats...")
    if backend == "bokeh":
        return plot_stats_bokeh(*args, **kwargs)
    elif backend == "mpl":
        return plot_stats_mpl(*args, **kwargs)
    else:
        raise ValueError("Backend [%s] not recognized" % backend)


def plot_channel_stats(channel_stats, folder, figsize=(12, 8)):

    taxis = channel_stats["time"]
    for statname, values in channel_stats.items():
        if statname == "time":
            continue
        basename = statname.replace(" ", "_")
        basename = "channel_stats_{}.png".format(basename)
        filename = os.path.join(folder, basename)
        fig = plt.figure(figsize=figsize)
        ax = fig.add_subplot(111)
        ax.plot(taxis, values)
        ax.set_title(statname)
        ax.set_xlabel("Time")
        fig.savefig(filename)
        plt.close(fig)


def plot_stats_mpl(stats, folder, skip=["lifetime", "time"]):
    fig = plt.figure(figsize=(12, 8))
    ax = fig.add_subplot(111)

    lengths = [stat["lifetime"] for stat in stats.values()]
    ax.hist(lengths, bins=20)
    ax.set_xlabel("Track lifetime")
    ax.set_ylabel("Count")
    fig.savefig(os.path.join(folder, "results_analysis_track_lifetimes.png"))
    plt.close(fig)

    fig = {}
    ax = {}
    timevals = {}
    for statname in next(iter(stats.values())):
        if statname in skip:
            continue
        fig[statname] = plt.figure(figsize=(12, 8))
        ax[statname] = fig[statname].add_subplot(111)

    for track_id, stat in sorted(stats.items()):
        timevals[track_id] = stat["time"]
        for statname in ax:
            values = stat[statname]
            ax[statname].plot(timevals[track_id], values, label=track_id)

    for name, axnow in ax.items():
        axnow.set_xlabel("Time (frames)")
        axnow.set_ylabel(name.capitalize())

    for figname, fignow in fig.items():
        print(figname)
        fname = "results_analysis_{}.png".format(
            figname.replace("/", "_over_").replace(" ", "_")
        )
        fignow.savefig(os.path.join(folder, fname))
        plt.close(fignow)


def plot_stats_bokeh(stats):

    raise NotImplementedError("This functionality is not yet complete")
    fig = pltb.figure()

    for track_id, stat in sorted(stats.items()):
        # For now is just mean intensity & time
        time_int = np.array(stat)
        color = plt.cm.jet(track_id / (len(stats) - 1))
        color = tuple(int(255 * c) for c in color[:3])
        print(color)
        color = [color for i in range(len(stat))]
        fig.line(
            time_int[:, 0],
            time_int[:, 1],
            line_color=color,
            # source=
        )

    # fig.add_tools(
    #    HoverTool(
    #        tooltips=['id':"@label"],
    #    )
    # )

    fig.xaxis.axis_label = "Time (frames)"
    fig.yaxis.axis_label = "Mean intensity"

    print("Saving bokeh plot...")
    bokeh.io.output_file(
        "results_analysis_mean_intensity.html",
        mode="inline",
        title="Mean intensity results",
    )
    pltb.show(fig)


def plot_channel_kymographs(channel_profiles, output_basename):
    """
    Generates kymograph plots from channel profiles
    """
    for i_chan, kymo in channel_profiles:
        plt.figure("Channel region %d" % i_chan, figsize=(12, 8))
        plt.imshow(kymo)
        plt.xlabel("x-axis")
        plt.ylabel("time")
        title = "Channel region %d ~kymograph" % i_chan
        title += " (max intensity projection in y over time)"
        plt.title(title)
        plt.savefig("{}_{}.png".format(output_basename, i_chan))
        plt.close()


def plot_and_save_focal_strength(
    strength, median=None, threshold=None, twosided=True, filename=None
):
    plt.figure()
    plt.plot(strength, "-b", lw=2)
    if (median is not None) and (threshold is not None):
        if twosided:
            plt.axhspan(
                median - np.abs(threshold),
                median + np.abs(threshold),
                linestyle="--",
                color="r",
                alpha=0.4,
                linewidth=2,
            )
        else:
            plt.axhline(
                median + threshold, linestyle="--", color="r", linewidth=2
            )
    plt.grid(True)
    if filename is None:
        return fig
    else:
        plt.savefig(filename)
        plt.close()


def save_detection_overlay(filename, image, cell, channel, well, well0=None):
    """
    Saves detection results, mainly for debugging purposes
    """
    image = image.copy() - image.min()
    image /= image.max()
    image_rgb = (255 * np.tile(image[..., None], (1, 1, 3))).astype("uint8")
    draw_mask_outline_into_image(
        image_rgb, cell, np.array([255, 0, 0]), alpha=1.0
    )
    draw_mask_outline_into_image(
        image_rgb, channel, np.array([0, 255, 0]), alpha=1.0
    )
    if well0 is not None:
        draw_mask_outline_into_image(
            image_rgb,
            (well0 == 1) | (well == 3),
            np.array([0, 255, 255]),
            alpha=0.6,
            thickness=1,
        )
        draw_mask_outline_into_image(
            image_rgb,
            (well0 == 2) | (well == 3),
            np.array([255, 0, 255]),
            alpha=0.6,
            thickness=1,
        )
    draw_mask_outline_into_image(
        image_rgb, well, np.array([0, 0, 255]), alpha=1.0
    )
    im_pil = Image.fromarray(image_rgb)
    draw = ImageDraw.Draw(im_pil)

    im_pil.save(filename)


def draw_mask_outline_into_image(
    image, mask, colour, thickness=2, inplace=True, alpha=1.0
):
    """
    Does basic erosion to get outline of mask,
    draws in values to image
    """
    if not inplace:
        raise NotImplementedError("Code me if needed")
    mask = mask.copy()
    mask[ndi.binary_erosion(mask > 0, structure=skmo.disk(thickness))] = 0
    image[mask > 0] = (1 - alpha) * image[mask > 0] + alpha * colour
