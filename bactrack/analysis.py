"""analysis.py

Analysis

J. Metz <metz.jp@gmail.com>
"""
import tqdm
import traceback
import skimage.measure as skmeas
import numpy as np
from bactrack.tracking import track_channels
from bactrack.utility import logger


def analyse_cells(cells, bgs, data, dark_count_correction=0, taxis=None):
    logger.info("Analysing cells")
    stats = {}

    if taxis is None:
        logger.info("  taxis not given, using arange")
        taxis = np.arange(len(data))

    for tpoint, cell, im, bg in zip(taxis, cells, data, bgs):
        statsnow = analyse_frame(
            cell, bg, im, dark_count_correction=dark_count_correction
        )
        stats = update_cell_stats(stats, statsnow, tpoint)

        props = skmeas.regionprops(cell)
        ids = [p.label for p in props]

    # Make arrays
    for k in stats:
        for statname in stats[k]:
            stats[k][statname] = np.array(stats[k][statname])

    # Add lifetimes
    for k, stat in stats.items():
        # Get length of the first stat
        # stats[k]['lifetime'] = len(next(iter(stat.values())))
        # Get difference between last and first timepoint + 1
        stats[k]["lifetime"] = stat["time"][-1] - stat["time"][0] + 1
    return stats


def analyse_channel_fluorescence(data, channels, taxis=None):
    """
    Channels contains two candidate channel regions

    * track the regions
    * analyse which one is the actual channel from the time series
    * return the time-series for the channel
    """
    try:
        channels = track_channels(channels)
    except Exception:
        logger.critical("Failed to track channels")
        logger.critical("Traceback:\n%s" % traceback.format_exc())
        raise Exception(
            "Failed to track channels; error was:\n\n%s"
            % traceback.format_exc()
        )

    # Get mean fluorescence over time for each channel
    means = np.zeros((2, len(data)))
    for tindex, (image, channel) in enumerate(zip(data, channels)):
        props = skmeas.regionprops(channel, image, coordinates="xy")
        means[:, tindex] = [prop.mean_intensity for prop in props]

    # Determine which one is the actual channel; it should have the
    # largest largest range in intensity values.
    # Use standard deviation as a metric
    stddev = np.std(means, axis=1)
    ind_channel = np.argmax(stddev)

    means = means[ind_channel]

    if taxis is None:
        taxis = np.arange(len(means))

    stats = {
        "time": taxis,
        "raw mean intensity over time": means,
        "normalised mean intensity over time": means / means[0],
    }
    return stats


def analyse_channel_fluorescence_kymograph(data, channels, taxis=None):
    """
    Generate kymographs for channel profiles
    """
    profs = []
    for tindex, (image, channel) in enumerate(zip(data, channels)):
        props = skmeas.regionprops(channel, image, coordinates="xy")

        profs.append([])
        for prop in props:
            # Super basic max projection
            profs[-1].append((image * (channel == prop.label)).max(axis=0))

    if taxis is None:
        taxis = np.arange(len(means))

    profiles = [[prof[index] for prof in profs] for index in len(channels)]

    raise NotImplementedError(
        "Still needs to return taxis / fill missing time data"
    )

    return profiles


def analyse_frame(cell, bg, im, dark_count_correction=0):
    props = {
        p.label: p for p in skmeas.regionprops(cell, im, coordinates="xy")
    }
    props_bg = [
        {p.label: p for p in skmeas.regionprops(bgnow, im, coordinates="xy")}
        for bgnow in bg
    ]

    """
    idsbg = [np.unique(bgn[bgn > 0]) for bgn in bg]
    print(  # "  len props_bg", len(props_bg),
            # "  |  len props_bg[0]", len(props_bg[0]),
          "  |  92 in props_bg[0]",
          "YES" if 92 in [p.label for p in props_bg[0]] else "NO",
          "  |  92 in props_bg[1]",
          "YES" if 92 in [p.label for p in props_bg[1]] else "NO",
          "  |  92 in either label image",
          # "YES" if any(92 in idbg for idbg in idsbg) else "NO",
          [92 in idbg for idbg in idsbg],
          )
    print("Difference between props.labels and props_bg.labels")
    print((ids | idsbg[0]).difference(ids & idsbg[0]))
    print((ids | idsbg[1]).difference(ids & idsbg[1]))
    """

    stats = {}
    for label, prop in props.items():
        dcc_mean = prop.mean_intensity - dark_count_correction
        means_bg = [
            propbg[label].mean_intensity
            for propbg in props_bg
            if label in propbg
        ]
        bg_raw = np.mean(means_bg)
        dcc_bg = bg_raw - dark_count_correction
        stats[prop.label] = {
            "mean raw": prop.mean_intensity,
            "mean dark-count corrected": dcc_mean,
            "bg raw": bg_raw,
            "bg dark-count corrected": dcc_bg,
            "dcc mean / dcc bg": dcc_mean / dcc_bg,
        }

    """
    print(
        "  IN analyse_frame, 92 in stats:",
        "YES" if 92 in stats else "NO",
        "  |  92 in props:",
        "YES" if 92 in ids else "NO",
    )
    """
    return stats


def update_cell_stats(stats, statsnow, timepoint):
    for trajid, statsitem in statsnow.items():
        stats.setdefault(trajid, {})
        for statname, statvalue in statsitem.items():
            stats[trajid].setdefault(statname, [])
            stats[trajid][statname].append(statvalue)
        stats[trajid].setdefault("time", [])
        stats[trajid]["time"].append(timepoint)
    return stats
