"""results_renormalizer.py

Script to reformat results

J. Metz <metz.jp@gmail.com>
"""
import os
import argparse
import glob
import re
import pandas as pd
from PyQt5 import QtWidgets


def get_folder_gui():
    """
    Simple Qt5 based folder getter
    """
    app = QtWidgets.QApplication([])
    folder = QtWidgets.QFileDialog.getExistingDirectory(
        parent=None,
        caption="Select root data folder"
    )
    return folder


def find_all_results_csvs(folder):
    """
    Get all files names results_table_full.csv
    under the given root folder
    """
    # return sorted(glob.glob(
    #    os.path.join(folder, "**", "results_table_full.csv")))
    files = []
    for root, folders, filenames in os.walk(folder):
        files.extend([
            os.path.join(root, filename)
            for filename in filenames
            if filename == "results_table_full.csv"
        ])
    return sorted(files)


def process_file(filename, timepoint_end=80):
    """
    Load the csv file, and create a new results file
    """
    table_old = pd.read_csv(filename)
    old_index = table_old.iloc[:,0]
    table = pd.DataFrame()
    # A) Get D(t) - D(0) / (D(end) - D(0)) for channel
    channel = table_old["Channel raw mean intensity over time"].values
    denom = channel[old_index == timepoint_end] - channel[0]
    channel_normed = (channel - channel[0]) / denom
    table['channel normalized'] = pd.Series(channel_normed, index=old_index)
    table['channel raw'] = pd.Series(channel, index=old_index)

    # B) Get ((d - dbg) - (d0 - dbg0)) / (D(end) - D(0))
    for column in table_old.filter(
            regex=(r"\d+ mean dark-count corrected")).columns:
        num = re.findall(r"^\d+", column)[0]
        d_cell = table_old[column].values
        d_bg = table_old[f"{num} bg dark-count corrected"].values
        data_unnormed = ((d_cell - d_bg)-(d_cell[0] - d_bg[0]))
        data_normed = data_unnormed / denom
        # data_normed_just_D0 = data_unnormed / channel[0]
        newname_normed = f"{num} bg subtracted and normed by channel"
        table[newname_normed] = pd.Series(data_normed, index=old_index)
        # Additional columns:
        # * one with just the numerator for
        #   the cells (so no division by (D(400)-D(0)))
        newname_unnormed = f"{num} bg subtracted unnormed"
        table[newname_unnormed] = pd.Series(data_unnormed, index=old_index)
        # * another column where this is divided by
        #   just D(0) rather than (D(400)-D(0))?
        # newname_normed_D0 = f"{num} bg subtracted normed just D0"
        # table[newname_normed_D0] = data_normed_just_D0

    folder = os.path.dirname(filename)
    newfilename = os.path.join(folder, "results_table_final.csv")
    table.to_csv(newfilename)


def create_parser():
    parser = argparse.ArgumentParser()
    parser.add_argument("folder", help="A positional input", nargs="?")
    return parser


def main():
    """
    Main batch processing function
    """
    args = create_parser().parse_args()
    folder = args.folder if args.folder else get_folder_gui()
    print("Processing folder:", folder)
    files = find_all_results_csvs(folder)
    print(f"  found {len(files)} files to process...")
    for filename in files:
        foldernow = os.path.basename(os.path.dirname(filename))
        print(f"    processing results folder {foldernow}")
        process_file(filename)
    print("Done!")


if __name__ == '__main__':
    main()
