# Overview

This sub-project is for attempts to do well
tracking without wells; i.e. fluoresence channel only. 

## Approaches

The approaches to consider are

* Basic single particle tracking, i.e. 
  replace trackmate-like functionality
* Adapt the current momanalysis framework to 
  be able to detect wells even using just fluorescence

## Details

### Single Particle Tracking

Having worked on single particle tracking a lot, 
this should be easy to write. The code would consist
of the usual detection & segmentation followed by tracking
cycles. 

### Well detection on fluoresence channel

Performing well detection on the fluorescence channel 
alone is conceptually challenging, but if the wells 
are well populated, this is still feasible. 

